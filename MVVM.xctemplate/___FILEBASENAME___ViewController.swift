//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//___COPYRIGHT___
//

import UIKit
import RxSwift
import RxCocoa

final class ___FILEBASENAME___ViewController: UIViewController {

	private let disposables = DisposeBag()
	private var viewModel: ___FILEBASENAME___ViewModel!
	private var navigationDelegate: ___FILEBASENAME___NavigationControllerDelegate!

	@IBOutlet private weak var viewConfigurator: ___FILEBASENAME___ViewConfigurator!

	func installDependencies(viewModel: ___FILEBASENAME___ViewModel, _ navigationDelegate: ___FILEBASENAME___NavigationControllerDelegate) {
		self.viewModel = viewModel
		self.navigationDelegate = navigationDelegate
	}

	override func viewDidLoad() {
		super.viewDidLoad()
		self.viewConfigurator.configure()
	}
	
}
