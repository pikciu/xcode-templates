//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//___COPYRIGHT___
//

import UIKit

final class ___FILEBASENAME___NavigationController: ___FILEBASENAME___NavigationControllerDelegate {
	private weak var viewController: ___FILEBASENAME___ViewController?

	init(viewController: ___FILEBASENAME___ViewController) {
		self.viewController = viewController
	}
	
}