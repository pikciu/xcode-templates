//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//___COPYRIGHT___
//

import Foundation
import RxSwift

final class ___FILEBASENAME___ViewModel {

	private let operationHandler: OperationHandler

	init(operationHandler: OperationHandler) {
		self.operationHandler = operationHandler
	}	
}