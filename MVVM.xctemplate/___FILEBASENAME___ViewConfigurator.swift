//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//___COPYRIGHT___
//

import UIKit

final class ___FILEBASENAME___ViewConfigurator: NSObject {

	@IBOutlet private weak var viewController: ___FILEBASENAME___ViewController!

	func configure() {
		
	}
}
