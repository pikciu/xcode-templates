//
//  ___FILEBASENAME___Provider.swift
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//___COPYRIGHT___
//


import Foundation

final class ___FILEBASENAME___Provider: ContextProviding {
	typealias TContext = ___FILEBASENAME___
    
    func execute<TOut, O: ContextOperational where O.TContext == TContext, O.TOut == TOut>(operation: O) -> TOut {
        fatalError("Not implemented")
    }
}