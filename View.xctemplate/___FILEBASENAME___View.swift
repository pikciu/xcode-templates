//
//  ___FILEBASENAME___Cell.swift
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//___COPYRIGHT___
//

import UIKit

@IBDesignable
final class ___FILEBASENAME___View: UIView {

	required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setupXib()
#if !TARGET_INTERFACE_BUILDER
#endif
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupXib()
#if !TARGET_INTERFACE_BUILDER
#endif
    }
}

extension ___FILEBASENAME___View: NibLoadableView {

}