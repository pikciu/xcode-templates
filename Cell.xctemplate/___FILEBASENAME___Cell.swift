//
//  ___FILEBASENAME___Cell.swift
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//___COPYRIGHT___
//

import UIKit

final class ___FILEBASENAME___Cell: UITableViewCell {
    
}

extension ___FILEBASENAME___Cell: NibLoadableView {

}

extension ___FILEBASENAME___Cell: Configurable {
	func configure(model: AnyObject) {

    }
}